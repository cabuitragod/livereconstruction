package unal.od.livereconstruction;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JRadioButton;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.util.prefs.Preferences;
import java.awt.event.ActionEvent;

public class MainFrame extends JFrame implements PreferenceKeys {

	private JPanel contentPane;

	// Propagation parameters
	private float lambda; // Wavelenght
	private float z; // Distance
	private float z0; // Illumination distance
	private float imageWidth;
	private float imageHeight;
	// Visualization parameters
	/// 0 - Intensity
	/// 1 - Amplitude
	/// 2 - Phase
	private int viewStyle;

	private final Preferences pref;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {

		pref = Preferences.userNodeForPackage(getClass());
		loadPreferences();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel mainPanel = new JPanel();
		contentPane.add(mainPanel, BorderLayout.CENTER);
		mainPanel.setLayout(new BorderLayout(0, 0));

		JPanel cameraPanel = new JPanel();
		mainPanel.add(cameraPanel, BorderLayout.CENTER);
		cameraPanel.setLayout(new BorderLayout(0, 0));

		JPanel controlPanel = new JPanel();
		mainPanel.add(controlPanel, BorderLayout.EAST);
		controlPanel.setLayout(new GridLayout(3, 1, 0, 0));

		JPanel control_initPanel = new JPanel();
		controlPanel.add(control_initPanel);
		control_initPanel.setLayout(new GridLayout(0, 1, 0, 0));

		JButton setupButton = new JButton("Setup");
		setupButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setupButtonActionPerformed();
			}
		});
		control_initPanel.add(setupButton);

		JButton reconstructButton = new JButton("Reconstruction");
		reconstructButton.setEnabled(false);
		control_initPanel.add(reconstructButton);

		JPanel control_visualPanel = new JPanel();
		control_visualPanel.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		controlPanel.add(control_visualPanel);
		control_visualPanel.setLayout(new BorderLayout(0, 0));

		JLabel lblVisualization = new JLabel("Visualization");
		lblVisualization.setHorizontalAlignment(SwingConstants.CENTER);
		control_visualPanel.add(lblVisualization, BorderLayout.NORTH);

		JPanel visualizationPanel = new JPanel();
		control_visualPanel.add(visualizationPanel, BorderLayout.CENTER);
		visualizationPanel.setLayout(new GridLayout(3, 1, 0, 0));

		JRadioButton intensityRadioBtn = new JRadioButton("Intensity");
		intensityRadioBtn.setHorizontalAlignment(SwingConstants.LEFT);
		visualizationPanel.add(intensityRadioBtn);

		JRadioButton amplitudeRadioBtn = new JRadioButton("Amplitude");
		visualizationPanel.add(amplitudeRadioBtn);

		JRadioButton phaseRadioBtn = new JRadioButton("Phase");
		visualizationPanel.add(phaseRadioBtn);

		ButtonGroup visualizationButtons = new ButtonGroup();
		visualizationButtons.add(intensityRadioBtn);
		visualizationButtons.add(amplitudeRadioBtn);
		visualizationButtons.add(phaseRadioBtn);

		JPanel control_endPanel = new JPanel();
		controlPanel.add(control_endPanel);
		control_endPanel.setLayout(new GridLayout(2, 1, 0, 0));

		JButton snapButton = new JButton("Snapshot");
		snapButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Debugging print the values
				System.out.println("lambda = " + Float.toString(lambda));
				System.out.println("distance = " + Float.toString(z));
				System.out.println("sourcedistance = " + Float.toString(z0));
				System.out.println("width = " + Float.toString(imageWidth));
				System.out.println("height = " + Float.toString(imageHeight));
			}
		});
		control_endPanel.add(snapButton);

		JButton shutdownButton = new JButton("Shutdown");
		control_endPanel.add(shutdownButton);
	}

	/**
	 * Loads all the preferences available
	 */
	protected void loadPreferences() {
		// Microscope selection
		loadMicroscopePreferences();
		// Camera settings
		loadCameraPreferences();
		// Reconstruction settings
		loadReconstructionPreferences();
		// Visualization settings
		loadVisualizationPreferences();
		// Enabled buttons
		loadEnabledButtonsPreferences();
	}

	/**
	 * Loads preferences related to the microscope selection
	 */
	private void loadMicroscopePreferences() {
		pref.getInt(MICROSCOPE_MODE, 0);
	}

	/**
	 * Loads preferences related to camera settings
	 */
	private void loadCameraPreferences() {
		pref.get(CAMERA_INPUT, "");
		pref.get(CAMERA_EXPOSURE, "");
		pref.get(CAMERA_GAIN, "");
		pref.get(CAMERA_GAMMA_CORRECTION, "");
	}

	/**
	 * Loads preferences related to reconstruction settings
	 */
	private void loadReconstructionPreferences() {
		lambda = Float.parseFloat(pref.get(LAMBDA, "533"));
		z = Float.parseFloat(pref.get(DISTANCE, "0.0"));
		z0 = Float.parseFloat(pref.get(SOURCE_DISTANCE, "0"));
		imageWidth = Float.parseFloat(pref.get(INPUT_WIDTH, "0.0"));
		imageHeight = Float.parseFloat(pref.get(INPUT_HEIGHT, "0.0"));
	}

	/**
	 * Loads preferences related to visualization preferences
	 */
	private void loadVisualizationPreferences() {
		pref.get(VIEW_INTENSITY, "");
		pref.get(VIEW_AMPLITUDE, "");
		pref.get(VIEW_PHASE, "");
	}

	/**
	 * Loads preferences related to enabled buttons
	 */
	private void loadEnabledButtonsPreferences() {
		pref.get(RECONSTRUCTION_ENABLED, "");
		pref.get(SNAPSHOT_ENABLED, "");
		pref.get(SHUTDOWN_ENABLED, "");
	}

	protected void setupButtonActionPerformed() {
		try {
			SetupFrame frame = new SetupFrame(this);
			frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * DEBUGGING: Sets preference values to correct dumb mistakes
	 */
	private void inyectPreferences() {
		pref.put(LAMBDA, "533");
		pref.put(DISTANCE, "0.0");
		pref.put(SOURCE_DISTANCE, "0.0");
		pref.put(INPUT_HEIGHT, "0.0");
		pref.put(INPUT_WIDTH, "0.0");
	}

}
