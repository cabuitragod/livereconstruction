package unal.od.livereconstruction;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.prefs.Preferences;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.Font;

public class SetupFrame extends JFrame implements PreferenceKeys {

	private JPanel contentPane;
	private JTextField wavelenghtField;
	private JTextField distanceField;
	private JTextField sourceDistanceField;
	private JTextField widthField;
	private JTextField heightField;

	// Microscope selection
	/// 0 - DHM
	/// 1 - DLHM
	private int microscopeType;

	// Camera settings
	private int exposure;
	private int gain;
	private int gammaFactor;

	private Font normalFont;
	private Font boldFont;

	private final MainFrame parent;
	private final Preferences pref;
	private JButton dhmButton;
	private JButton dlhmButton;

	/**
	 * Launch the application.
	 * 
	 * public static void main(String[] args) { EventQueue.invokeLater(new
	 * Runnable() { public void run() { try { SetupFrame frame = new SetupFrame();
	 * frame.setVisible(true); } catch (Exception e) { e.printStackTrace(); } } });
	 * }
	 */

	/**
	 * Create the frame.
	 */
	public SetupFrame(MainFrame parent) {
		setResizable(false);
		pref = Preferences.userNodeForPackage(getClass());
		this.parent = parent;

		normalFont = new Font("Tahoma", Font.PLAIN, 11);
		boldFont = new Font("Tahoma", Font.BOLD, 11);

		initComponents();
		loadPreferences();
	}

	private void initComponents() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 320, 320);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JTabbedPane setupPanel = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(setupPanel, BorderLayout.CENTER);

		JPanel microscopeSelectionPanel = new JPanel();
		setupPanel.addTab("Microscope", null, microscopeSelectionPanel, null);
		microscopeSelectionPanel.setLayout(new BorderLayout(0, 0));

		JPanel microscopeSelection_finalPanel = new JPanel();
		microscopeSelectionPanel.add(microscopeSelection_finalPanel, BorderLayout.SOUTH);
		microscopeSelection_finalPanel.setLayout(new BorderLayout(0, 0));

		JButton microscopeSelection_nextButton = new JButton("Next >");
		microscopeSelection_nextButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setupPanel.setSelectedIndex(1);
				updateMicroscopePrefereces();
			}
		});
		microscopeSelection_finalPanel.add(microscopeSelection_nextButton, BorderLayout.EAST);

		JPanel microscopeSelection_optionsPanel = new JPanel();
		microscopeSelectionPanel.add(microscopeSelection_optionsPanel, BorderLayout.CENTER);
		microscopeSelection_optionsPanel.setLayout(new GridLayout(1, 2, 0, 0));

		JPanel dhmPanel = new JPanel();
		microscopeSelection_optionsPanel.add(dhmPanel);
		dhmPanel.setLayout(new BorderLayout(0, 0));

		dhmButton = new JButton("Traditional DHM");
		dhmPanel.add(dhmButton, BorderLayout.SOUTH);

		JLabel dhmImage = new JLabel("");
		// TODO Replace for resource-linked image
		dhmImage.setIcon(
				new ImageIcon("C:\\Users\\Optodigital\\EclipseProjects\\RealTimeReconstruction\\resources\\DHM.png"));
		dhmPanel.add(dhmImage, BorderLayout.CENTER);

		JPanel dlhmPanel = new JPanel();
		microscopeSelection_optionsPanel.add(dlhmPanel);
		dlhmPanel.setLayout(new BorderLayout(0, 0));

		dlhmButton = new JButton("Lensless DHM");
		dlhmPanel.add(dlhmButton, BorderLayout.SOUTH);

		JLabel dlhmImage = new JLabel("");
		// TODO Replace for resource-linked image
		dlhmImage.setIcon(
				new ImageIcon("C:\\Users\\Optodigital\\EclipseProjects\\RealTimeReconstruction\\resources\\DLHM.jpg"));
		dlhmPanel.add(dlhmImage, BorderLayout.CENTER);

		dhmButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				microscopeType = 0;
				dlhmButton.setFont(normalFont);
				dhmButton.setFont(boldFont);
			}
		});

		dlhmButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				microscopeType = 1;
				dlhmButton.setFont(boldFont);
				dhmButton.setFont(normalFont);
			}
		});

		JPanel cameraSetupPanel = new JPanel();
		setupPanel.addTab("Camera", null, cameraSetupPanel, null);
		cameraSetupPanel.setLayout(new BorderLayout(0, 0));

		JPanel cameraSetup_selectionPanel = new JPanel();
		cameraSetupPanel.add(cameraSetup_selectionPanel, BorderLayout.NORTH);
		cameraSetup_selectionPanel.setLayout(new BorderLayout(0, 0));

		JButton cameraTestButton = new JButton("Test live");
		cameraSetup_selectionPanel.add(cameraTestButton, BorderLayout.EAST);

		JComboBox cameraAvailability = new JComboBox();
		cameraSetup_selectionPanel.add(cameraAvailability, BorderLayout.CENTER);

		JPanel cameraSetup_controlPanel = new JPanel();
		cameraSetupPanel.add(cameraSetup_controlPanel, BorderLayout.CENTER);
		cameraSetup_controlPanel.setLayout(new GridLayout(4, 1, 0, 10));

		JPanel cameraSetup_exposurePanel = new JPanel();
		cameraSetup_controlPanel.add(cameraSetup_exposurePanel);
		cameraSetup_exposurePanel.setLayout(new BorderLayout(0, 0));

		JLabel lblExposure = new JLabel("Exposure");
		lblExposure.setHorizontalAlignment(SwingConstants.LEFT);
		cameraSetup_exposurePanel.add(lblExposure, BorderLayout.NORTH);

		JSeparator separator = new JSeparator();
		cameraSetup_exposurePanel.add(separator, BorderLayout.SOUTH);

		JSpinner exposureSpinner = new JSpinner();
		cameraSetup_exposurePanel.add(exposureSpinner, BorderLayout.EAST);

		JSlider exposureSlider = new JSlider();
		cameraSetup_exposurePanel.add(exposureSlider, BorderLayout.CENTER);

		exposureSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				exposure = (int) exposureSpinner.getValue();
				exposureSlider.setValue(exposure);
			}
		});
		exposureSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				exposure = exposureSlider.getValue();
				exposureSpinner.setValue(exposure);
			}
		});

		JPanel cameraSetup_gainPanel = new JPanel();
		cameraSetup_controlPanel.add(cameraSetup_gainPanel);
		cameraSetup_gainPanel.setLayout(new BorderLayout(0, 0));

		JLabel lblGain = new JLabel("Gain");
		lblGain.setHorizontalAlignment(SwingConstants.LEFT);
		cameraSetup_gainPanel.add(lblGain, BorderLayout.NORTH);

		JSeparator separator_1 = new JSeparator();
		cameraSetup_gainPanel.add(separator_1, BorderLayout.SOUTH);

		JSpinner gainSpinner = new JSpinner();
		cameraSetup_gainPanel.add(gainSpinner, BorderLayout.EAST);

		JSlider gainSlider = new JSlider();
		cameraSetup_gainPanel.add(gainSlider, BorderLayout.CENTER);

		gainSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				gain = (int) gainSpinner.getValue();
				gainSlider.setValue(gain);
			}
		});
		gainSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				gain = gainSlider.getValue();
				gainSpinner.setValue(gain);
			}
		});

		JPanel cameraSetup_gammaPanel = new JPanel();
		cameraSetup_controlPanel.add(cameraSetup_gammaPanel);
		cameraSetup_gammaPanel.setLayout(new BorderLayout(0, 0));

		JLabel lblGammaCorrection = new JLabel("Gamma correction");
		lblGammaCorrection.setHorizontalAlignment(SwingConstants.LEFT);
		cameraSetup_gammaPanel.add(lblGammaCorrection, BorderLayout.NORTH);

		JSeparator separator_2 = new JSeparator();
		cameraSetup_gammaPanel.add(separator_2, BorderLayout.SOUTH);

		JSpinner gammaSpinner = new JSpinner();
		cameraSetup_gammaPanel.add(gammaSpinner, BorderLayout.EAST);

		JSlider gammaSlider = new JSlider();
		cameraSetup_gammaPanel.add(gammaSlider, BorderLayout.CENTER);

		gammaSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				gammaFactor = (int) gammaSpinner.getValue();
				gammaSlider.setValue(gammaFactor);
			}
		});
		gammaSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				gammaFactor = gammaSlider.getValue();
				gammaSpinner.setValue(gammaFactor);
			}
		});

		JPanel cameraSetup_Placeholder = new JPanel();
		cameraSetup_controlPanel.add(cameraSetup_Placeholder);
		cameraSetup_Placeholder.setLayout(new BorderLayout(0, 0));

		JSpinner spinner_3 = new JSpinner();
		spinner_3.setEnabled(false);
		cameraSetup_Placeholder.add(spinner_3, BorderLayout.EAST);

		JSlider slider = new JSlider();
		slider.setEnabled(false);
		cameraSetup_Placeholder.add(slider, BorderLayout.CENTER);

		JLabel lblPlaceholder = new JLabel("Placeholder");
		cameraSetup_Placeholder.add(lblPlaceholder, BorderLayout.NORTH);

		JPanel cameraSetup_finalpanel = new JPanel();
		cameraSetupPanel.add(cameraSetup_finalpanel, BorderLayout.SOUTH);
		cameraSetup_finalpanel.setLayout(new BorderLayout(0, 0));

		JButton cameraSetup_nextButton = new JButton("Next >");
		cameraSetup_nextButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setupPanel.setSelectedIndex(2);
				updateConfig();
			}
		});
		cameraSetup_finalpanel.add(cameraSetup_nextButton, BorderLayout.EAST);

		JButton cameraSetup_backButton = new JButton("< Previous");
		cameraSetup_backButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setupPanel.setSelectedIndex(0);
			}
		});
		cameraSetup_finalpanel.add(cameraSetup_backButton, BorderLayout.WEST);

		JPanel reconstructionSetupPanel = new JPanel();
		setupPanel.addTab("Reconstruction", null, reconstructionSetupPanel, null);
		reconstructionSetupPanel.setLayout(new BorderLayout(0, 0));

		JPanel reconstructionSetup_finalPanel = new JPanel();
		reconstructionSetupPanel.add(reconstructionSetup_finalPanel, BorderLayout.SOUTH);
		reconstructionSetup_finalPanel.setLayout(new BorderLayout(0, 0));

		JButton reconstructionSetup_backButton = new JButton("< Previous");
		reconstructionSetup_backButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setupPanel.setSelectedIndex(1);
			}
		});
		reconstructionSetup_finalPanel.add(reconstructionSetup_backButton, BorderLayout.WEST);

		JPanel reconstructionSetup_closeButtonsPanel = new JPanel();
		reconstructionSetup_finalPanel.add(reconstructionSetup_closeButtonsPanel, BorderLayout.EAST);
		reconstructionSetup_closeButtonsPanel.setLayout(new GridLayout(0, 2, 0, 0));

		JButton reconstructionSetup_applyButton = new JButton("Apply");
		reconstructionSetup_applyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateConfig();
			}
		});
		reconstructionSetup_closeButtonsPanel.add(reconstructionSetup_applyButton);

		JButton reconstructionSetup_FinishButton = new JButton("Finish");
		reconstructionSetup_FinishButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateConfig();
				dispose();
			}
		});
		reconstructionSetup_closeButtonsPanel.add(reconstructionSetup_FinishButton);

		JPanel reconstructionSetup_parametersPanel = new JPanel();
		reconstructionSetupPanel.add(reconstructionSetup_parametersPanel, BorderLayout.CENTER);
		reconstructionSetup_parametersPanel.setLayout(new BorderLayout(0, 0));

		JPanel reconstructionSetup_fourierPanel = new JPanel();
		reconstructionSetup_parametersPanel.add(reconstructionSetup_fourierPanel, BorderLayout.SOUTH);
		reconstructionSetup_fourierPanel.setLayout(new GridLayout(1, 2, 0, 0));

		JLabel fourierSetupLabel = new JLabel("Fourier Transform Filter");
		fourierSetupLabel.setHorizontalAlignment(SwingConstants.CENTER);
		reconstructionSetup_fourierPanel.add(fourierSetupLabel);

		JPanel reconstructionSetup_fourierOptionsPanel = new JPanel();
		reconstructionSetup_fourierPanel.add(reconstructionSetup_fourierOptionsPanel);
		reconstructionSetup_fourierOptionsPanel.setLayout(new GridLayout(2, 1, 0, 0));

		JRadioButton automaticFourierRadioBtn = new JRadioButton("Automatic filter");
		reconstructionSetup_fourierOptionsPanel.add(automaticFourierRadioBtn);

		JRadioButton manualFourierRadioBtn = new JRadioButton("Manual ROI filter");
		reconstructionSetup_fourierOptionsPanel.add(manualFourierRadioBtn);

		ButtonGroup fourierButtons = new ButtonGroup();
		fourierButtons.add(automaticFourierRadioBtn);
		fourierButtons.add(manualFourierRadioBtn);

		JPanel reconstructionSetup_propagationPanel = new JPanel();
		reconstructionSetup_parametersPanel.add(reconstructionSetup_propagationPanel, BorderLayout.CENTER);
		reconstructionSetup_propagationPanel.setLayout(new GridLayout(5, 2, 10, 10));

		JLabel lblWavelenght = new JLabel("Wavelenght [nm]:");
		lblWavelenght.setHorizontalAlignment(SwingConstants.TRAILING);
		reconstructionSetup_propagationPanel.add(lblWavelenght);

		wavelenghtField = new JTextField();
		reconstructionSetup_propagationPanel.add(wavelenghtField);
		wavelenghtField.setColumns(10);

		JLabel lblReconstructionDistance = new JLabel("Reconst. distance [mm]:");
		lblReconstructionDistance.setHorizontalAlignment(SwingConstants.TRAILING);
		reconstructionSetup_propagationPanel.add(lblReconstructionDistance);

		distanceField = new JTextField();
		reconstructionSetup_propagationPanel.add(distanceField);
		distanceField.setColumns(10);

		JLabel lblDistanceFromSource = new JLabel("Dist. from source [mm]:");
		lblDistanceFromSource.setHorizontalAlignment(SwingConstants.TRAILING);
		reconstructionSetup_propagationPanel.add(lblDistanceFromSource);

		sourceDistanceField = new JTextField();
		reconstructionSetup_propagationPanel.add(sourceDistanceField);
		sourceDistanceField.setColumns(10);

		JLabel lblInputWidth = new JLabel("Input width [mm]:");
		lblInputWidth.setHorizontalAlignment(SwingConstants.TRAILING);
		reconstructionSetup_propagationPanel.add(lblInputWidth);

		widthField = new JTextField();
		reconstructionSetup_propagationPanel.add(widthField);
		widthField.setColumns(10);

		JLabel lblInputHeight = new JLabel("Input height [mm]:");
		lblInputHeight.setHorizontalAlignment(SwingConstants.TRAILING);
		reconstructionSetup_propagationPanel.add(lblInputHeight);

		heightField = new JTextField();
		reconstructionSetup_propagationPanel.add(heightField);
		heightField.setColumns(10);
	}

	protected void updateConfig() {
		updateMicroscopePrefereces();
		updateCameraPreferences();
		updateReconstructionPreferences();
		parent.loadPreferences();
	}

	/**
	 * Modifies preferences related to the microscope selection
	 */
	protected void updateMicroscopePrefereces() {
		pref.putInt(MICROSCOPE_MODE, microscopeType);
	}

	/**
	 * Modifies preferences related to camera settings
	 */
	private void updateCameraPreferences() {
		pref.put(CAMERA_INPUT, "");
		pref.put(CAMERA_EXPOSURE, "");
		pref.put(CAMERA_GAIN, "");
		pref.put(CAMERA_GAMMA_CORRECTION, "");
	}

	/**
	 * Modifies preferences related to reconstruction settings
	 */
	private void updateReconstructionPreferences() {
		pref.put(LAMBDA, wavelenghtField.getText());
		pref.put(DISTANCE, distanceField.getText());
		pref.put(SOURCE_DISTANCE, sourceDistanceField.getText());
		pref.put(INPUT_WIDTH, widthField.getText());
		pref.put(INPUT_HEIGHT, heightField.getText());
	}

	/**
	 * Loads preferences related to the microscope selection
	 */
	private void loadPreferences() {
		microscopeType = pref.getInt(MICROSCOPE_MODE, 0);
		if (microscopeType == 0) {
			dlhmButton.setFont(normalFont);
			dhmButton.setFont(boldFont);
		} else {
			dlhmButton.setFont(boldFont);
			dhmButton.setFont(normalFont);
		}

		pref.get(CAMERA_INPUT, "");
		pref.get(CAMERA_EXPOSURE, "");
		pref.get(CAMERA_GAIN, "");
		pref.get(CAMERA_GAMMA_CORRECTION, "");

		wavelenghtField.setText(pref.get(LAMBDA, "533"));
		distanceField.setText(pref.get(DISTANCE, "0"));
		sourceDistanceField.setText(pref.get(SOURCE_DISTANCE, "0"));
		widthField.setText(pref.get(INPUT_WIDTH, ""));
		heightField.setText(pref.get(INPUT_HEIGHT, ""));

		pref.get(RECONSTRUCTION_ENABLED, "");
		pref.get(SNAPSHOT_ENABLED, "");
		pref.get(SHUTDOWN_ENABLED, "");
	}

}
