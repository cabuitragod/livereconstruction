package unal.od.livereconstruction;

public interface PreferenceKeys {
	
	// SETUP FRAME
	/// Microscope selection
	final static String MICROSCOPE_MODE = "microscopeMode";
	/// Camera settings
	final static String CAMERA_INPUT = "cameraInput";
	final static String CAMERA_EXPOSURE = "cameraExposure";
	final static String CAMERA_GAIN = "cameraGain";
	final static String CAMERA_GAMMA_CORRECTION = "cameraGamma";
	/// Reconstruction settings
	final static String LAMBDA = "lambda";
    final static String DISTANCE = "distance";
    final static String SOURCE_DISTANCE = "sourceDistance";
    final static String INPUT_WIDTH = "inputWidth";
    final static String INPUT_HEIGHT = "inputHeight";
    
    // MAIN FRAME
    /// Visualization
    final static String VIEW_INTENSITY = "viewIntensity";
    final static String VIEW_AMPLITUDE = "viewAmplitude";
    final static String VIEW_PHASE = "viewPhase";
    /// Enabled buttons
    final static String RECONSTRUCTION_ENABLED = "isReconstructionEnabled";
    final static String SNAPSHOT_ENABLED = "isSnapshotEnabled";
    final static String SHUTDOWN_ENABLED = "isShutdownEnabled";

}
